#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Bientôt noël

import numpy as np
import matplotlib.pyplot as plt
import title as tt
import sys


def eval_f(x_):
    return np.cos(x_)

def int_eval_f(x_):
    return (-1)*np.sin(x_)

def integrate_trapezium(f_,a_,b_,n_):
    """ Integrate a function from a_ to b_ with the trapeziums method

    :param f_: The function to integrate
    :param a_: One of the bounds
    :param b_: The other bound
    :param n_: Number of trapeziums for the integration

    :type f_: fonction
    :type a_: float
    :type b_: float
    :type n_: int

    :Example:

    integrate_trapezium(eval_f, 0, 3.14, 1000)
    """
    min = a_
    max = b_
    if min > max: min, max = max, min
    dx = (max-min)/n_
    sum = 0

    for i in range(0,n_-1):
        f_min = f_(min+i*dx)
        f_max = f_(min+(i+1)*dx)
        if f_min>f_max: f_min,f_max = f_max,f_min
        sum += f_min*dx + 0.5*dx*(f_max-f_min)

    return sum

def integrate_rectangle(f_,a_,b_,h_):
    """ Integrate a function from a_ to b_ with the rectangle method

    :param f_: The function to integrate
    :param a_: One of the bounds
    :param b_: The other bound
    :param h_: Step of integration

    :type f_: fonction
    :type a_: float
    :type b_: float
    :type h_: float

    :Example:

    integrate_rectangle(eval_f, 0, 3.14, 0.1)
    """
    min, max = a_, b_
    if min > max: min, max = max, min
    sum = 0
    dx = (max - min)/n_

    for i in range (0, n_-1):
        sum += f_(min + i*dx)*dx

    return sum

def calcul_N_int(f_, a_, b_, n_, N_):
    """ Integrate a function from a_ to x that belongs to [a, b]

    :param f_: The function to integrate
    :param a_: One of the bounds
    :param b_: The other bound
    :param n_: Number of trapeziums for each integration
    :param N_: Number of points for the output function

    :type f_: fonction
    :type a_: float
    :type b_: float
    :type n_: int
    :type N_: int

    :Example:

    calcul_N_int(eval_f, 0, 3.14, 1000, 1000)
    """

    x, F_a_x = [], []
    min,max = a_,b_

    if min > max: min,max = max,min
    dx = abs((a_ - b_)/N_)
    
    print(dx)
    
    for i in range(N_):
        x.append(a_ + i*dx)
        F_a_x.append(integrate_trapezium(f_, a_, x[i], n_))

    return x, F_a_x

def average_imprecision(numeric_, analytic_):
    """ Compute the average imprecision between the analytic and the numeric solutions

    :param analytic_: The analytics values
    :param numeric_: The numerics values

    :type analytic_: list
    :type numeric_: list

    :Example:

    average_imprecision(analytic_values, numeric_values)
    """

    imprecision = []

    for i in range (len(numeric_)):
        imprecision.append((numeric_[i] - analytic_[i]))
    
    return sum(imprecision)/(len(imprecision))

def main():
    debug = False

    if "-d" in sys.argv:
        debug = True
    
    if debug:
        tt.show(tt.create("Intégration"))

    #===================#
    # Compute int_a^b f #
    #===================#

    """tt.show(tt.create("Calcul d'une valeur F(x)"))

    inf = float(input("Borne inférieure : "))
    sup = float(input("Borne supérieure : "))
    N = int(input("Nombre de pas : "))
        
    result = integrate_rectangle(eval_f,inf,sup,N),integrate_trapezium(eval_f,inf,sup,N)
    print("Rectangle: {}\nTrapezium: {}".format(str(result[0]),str(result[1])))"""

    #==================================#
    # Plotting the integrated function #
    #==================================#

    tt.show(tt.create("Tracé de l'intégrale d'une fonction"))

    inf = float(input("Borne inférieure : "))
    sup = float(input("Borne supérieure (x) : "))
    n = int(input("Nombre de trapèzes par intégration : "))
    N = int(input("Nombre de points pour le tracé de F(x) : "))

    x, F_x = calcul_N_int(eval_f, inf, sup, n, N)
    y = []
    h = (max(sup,inf)-min(sup,inf))/n

    for i in range (N):
        y.append(int_eval_f(x[i]))

    #==============================================================================#
    # Compute the difference between numeric and analytic solution for each points #
    #==============================================================================#
    
    average_error = average_imprecision(F_x, y)

    print("The average imprecision is : {}".format(average_error))

    # Plotting
    plt.figure()
    plt.suptitle("Computation of integration", fontsize=14, fontweight='bold')
    plt.title("Number of points: {}".format(N))
    plt.plot(x, F_x, 'or')
    plt.plot(x, F_x, '-b')
    plt.show(block = False)
    plt.show()
          
if __name__ == "__main__": main()
