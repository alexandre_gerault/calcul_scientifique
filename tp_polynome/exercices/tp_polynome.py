#! /usr/bin/env python3
# -*- conding: utf-8 -*-

import numpy as np

def main():
	a,b,c = float(input("Entrez un nombre")), float(input("Entrez un autre nombre")),float(input("Entrez un dernier nombre"))
	print("a={}, b={} et c={}".format(a,b,c))
	delta = b**2 - 4*a*c
	print("Delta = {}".format(delta))
	solutions_amount=0
	is_complex=False
	solutions=list()

	if(delta > 0):
		solutions_amount=2
		solutions.append((-b-np.sqrt(delta))/(2*a))
		solutions.append((-b+np.sqrt(delta))/(2*a))
	elif(delta == 0):
		solutions_amount=1
		solutions.append(-b/(2*a))
	elif(delta < 0):
		solutions_amount=2
		is_complex = True
		solutions.append(complex((-b)/(2*a), (-np.sqrt(-delta)/(2*a))))
		solutions.append(complex((-b)/(2*a), ( np.sqrt(-delta)/(2*a))))
	else:
		print("Bizarre bizarre...")
	
	sentence = "Nombre de solution"
	if(solutions_amount > 1): sentence += "s"
	
	sentence += " : "

	print(sentence, solutions_amount)
	if(is_complex):
		print("Solutions complexes")
	else:
		print("Solutions reelles")
	i = 0
	for solution in solutions:
		i+=1
		print("x_{} = {}".format(i, solution))
		
                
main()
