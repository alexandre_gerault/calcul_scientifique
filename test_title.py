#!/usr/bin/env python3

import title as tt
import sys

title = ""
is_title = False
for arg in sys.argv:
    if(is_title):
        title = arg
        is_title = False
    if(arg == "-t"):
        is_title = True

tt.show(tt.create(title))
