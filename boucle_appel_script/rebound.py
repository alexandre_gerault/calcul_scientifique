#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Mon Oct 22 2018

import title as tt
import sys
import os
import random

def main(args):

  n = 100

  if sys.platform == 'linux':
    string = [str(int(random.uniform(0,1000)) % 2) for i in range(n)]
    for element in string:
      sys.stdout.write(element)
    sys.stdout.flush()
    os.system('./entry.py -c')

  return

if __name__ == "__main__": main(sys.argv)