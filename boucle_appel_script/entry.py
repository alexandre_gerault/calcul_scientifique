#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Mon Oct 22 2018

import title as tt
import sys
import os
import random
import time

def main(args):

  first_call = True

  if '-c' in args: first_call = False

  if first_call:
    tt.show(tt.create("MISERANDINI ATTACK!!!"))
    time.sleep(0.5)
    print("Attack is about to start...")
    time.sleep(1)

  n = 100

  if sys.platform == 'linux':
    string = [str(int(random.uniform(0,1000)) % 2) for i in range(n)]
    for element in string:
      sys.stdout.write(element)
    sys.stdout.flush()
    os.system('./rebound.py')

  return

if __name__ == "__main__": main(sys.argv)