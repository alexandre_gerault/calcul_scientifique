#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Christmas is coming

import random as rd
import matplotlib.pyplot as plt

def random_unif(inf_, sup_):
    return rd.random() * ( sup_ - inf_ ) + inf_

def main():
    rd.seed(1)
    x = []
    random_numbers = []

    inf = float(input("Low bound: "))
    sup = float(input("High bound: "))
    amount_of_numbers = int(input("Number of numbers: "))

    inf, sup = min(inf, sup), max(inf, sup)

    """for i in range(amount_of_numbers):
        x.append(i)
        random_numbers.append(random_unif(inf, sup))

    average = sum(random_numbers)/len(random_numbers)
    expected_average = (sup - inf)/2 + inf
    print("The average is: {}".format(average))
    print("Expected value: {}".format(expected_average))
    # print(random_numbers)

    plt.figure()
    plt.hist(random_numbers, bins=10)
    plt.show()"""
    
if __name__ == "__main__": main()
