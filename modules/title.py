def create(title):
    title_length = len(title) + 5
    stars = ""
    for i in range(1, title_length):
        stars += "*"

    formatted_title = "{} \n* {} *\n{}".format(stars, title, stars)

    return formatted_title

def show(title):
    print(title)
