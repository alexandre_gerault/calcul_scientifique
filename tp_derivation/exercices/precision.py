#!/usr/bin/env python3
# -*- codging: utf-8 -*-
# Alexandre Gérault
# Lundi 22 octobre 2018

import title as tt
import numpy as np
import matplotlib.pyplot as plt

def derivation(function, location, precision):
  if precision == 0:
    raise ZeroDivisionError('Precision can\'t be equal to zero!')
  
  return ( function(location + precision) - function(location - precision) ) / ( 2*precision )

def analytic_solution(x):
  return -np.sin(x)

def average_absolute(my_list):
  sum = 0
  for i in my_list:
    sum += np.absolute(i)

  return sum/len(my_list)

def difference_numeric_analytic(numeric_function, analytic_function, precision, range):
  x = range
  numeric_derivation = []
  analytic_derivation = []
  difference = []

  # Filling
  for i in x:
    numeric_computation = derivation(numeric_function, i, precision)
    analytic_computation = -np.sin(i)

    numeric_derivation.append(numeric_computation)
    analytic_derivation.append(analytic_computation)

    difference.append(numeric_computation - analytic_computation)
    
  return x, numeric_derivation, analytic_derivation, difference

def main():
  debug = True

  if debug:
    tt.show(tt.create("Numeric differentiation computation"))

  cos_range = np.arange(0,2*np.pi,(2*np.pi/100))[:]
    
  cos_derivation = difference_numeric_analytic(np.cos, analytic_solution, 0.1, cos_range)
  
  # Estimating difference between numeric and analytic method
  absolute_value_average = average_absolute(cos_derivation[3])
  print("Estimation of the difference between numeric and analytic method: {}".format(absolute_value_average))

  # Estimation of difference depending on the choosen precision
  h_0 = 10**(-6)
  h_list = []
  difference_h = []

  for i in range(25):
    h = h_0*(2**0.5)**i
    cos_derivation_h = difference_numeric_analytic(np.cos, analytic_solution, h, cos_range)
    difference_h.append(average_absolute(cos_derivation_h[3]))
    h_list.append(h)


  # Plotting
  plt.title("Computation of cosinus derivation")
  plt.plot(cos_derivation[0], cos_derivation[1], '-r')
  plt.plot(cos_derivation[0], cos_derivation[2], '-r')
  plt.show(block=False)
  plt.show()


  plt.figure()
  plt.title("Difference depending on precision")
  plt.plot(h_list, difference_h, '-')
  plt.xscale('log')
  plt.yscale('log')
  plt.show(block=False)
  plt.show()

  # Choosing the best precision:
  lowest_diff_index = difference_h.index(min(difference_h))
  best_precision = h_list[lowest_diff_index - 1]

  print("The best precision is: {}".format(best_precision))

  return

if __name__ == "__main__": main()