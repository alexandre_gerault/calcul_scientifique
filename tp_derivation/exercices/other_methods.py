#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Tue Oct 23 2018

import numpy as np
import title as tt
import matplotlib.pyplot as plt

def derivation_fm(f, x, h):
  if h == 0:
    raise ZeroDivisionError('Precision can\'t be equal to zero!')
  return (-f(x+2*h) + 8*f(x+h)- 8*f(x-h) + f(x-2*h))/(12*h)

def derivation_sm(f, x, h):
  if h == 0:
    raise ZeroDivisionError('Precision can\'t be equal to zero!')
  return (f(x+3*h) - 9*f(x+2*h) + 45*f(x+h) - 45*f(x-h)+9*f(x-2*h)-f(x-3*h))/(60*h)

def derivation_symetric_fm(f, x, h):
  if h == 0:
    raise ZeroDivisionError('Precision can\'t be equal to zero!')
  return (f(x+h) - f(x))/(2*h)

def derivation_symetric_sm(f, x, h):
  if h == 0:
    raise ZeroDivisionError('Precision can\'t be equal to zero!')
  return (-3*f(x) + 4*f(x+h) - f(x+2*h))/(2*h)

def derivation_symetric_tm(f, x, h):
  if h == 0:
    raise ZeroDivisionError('Precision can\'t be equal to zero!')
  return (-15*f(x) + 16*f(x+h) - f(x+4*h))/(12*h)


def average_absolute(my_list):
  sum = 0
  for i in my_list:
    sum += np.absolute(i)

  return sum/len(my_list)


def derivative_imprecision(numeric, analytic, h, range, method):
  """ Estimating the imprecision of the derivative method

  :param numeric: The numeric function to derivate
  :param analytic: The analytic function of the derivated function
  :param h: parameter injected into derivative method
  :param range: Range on which the derivate is computed
  :param method: Derivative method

  :type numeric: fonction
  :type analytic: fonction
  :type h: float
  :type range: list
  :type method: fonction

  :Example:

  derivative_imprecision(np.cos, solution, 0.1, np.arange(0,10), derivation)
  """

  x = range
  numeric_derivation = []
  analytic_derivation = []
  difference = []

  # Filling
  for i in x:
    numeric_computation = method(numeric, i, h)
    analytic_computation = analytic(i)

    numeric_derivation.append(numeric_computation)
    analytic_derivation.append(analytic_computation)

    difference.append(numeric_computation - analytic_computation)
    
  return difference

def analytic_solution(x):
  return -1*np.sin(x)

def main():
  derivative_methods = [derivation_fm, derivation_sm, derivation_symetric_fm, derivation_symetric_sm, derivation_symetric_tm]

  x = np.arange(0,2*np.pi,(2*np.pi/100))[:]
  function_to_derivate = np.cos

  for method in derivative_methods:
    difference_h = []
    h_list = []
    # Que sur x = pi/3 (point donné)
    for h in np.arange(0.001, 1, 0.001):
      derivated_function_difference = derivative_imprecision(function_to_derivate, analytic_solution, h, x, method)
      difference_h.append(average_absolute(derivated_function_difference))
      h_list.append(h)
    
    plt.figure()
    plt.title("Average imprecision depending on $h$ with {} method".format(method.__name__))
    plt.plot(h_list, difference_h, '-')
    plt.xscale('log')
    plt.yscale('log')
    plt.show(block=False)
    plt.show()

    # Choosing the best precision:
    lowest_diff_index = difference_h.index(min(difference_h))
    best_precision = h_list[lowest_diff_index - 1]

    print("The best precision for the {} method is: {}".format(method.__name__, best_precision))

if __name__ == "__main__": main()
