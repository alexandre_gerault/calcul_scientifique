#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# 19 octobre 2018

import random
import title as tt

def main():
    
    debug = True

    if debug:
        tt.show(tt.create("More or less game"))
    
    mystery_number = int(random.uniform(0,1000))
    user_input = int(input("What is mystery number? "))

    while user_input != mystery_number:
        if user_input > mystery_number:
            print("Try a smaller number")
        elif user_input < mystery_number:
            print("Try a bigger number")
        
        user_input = int(input("What is mystery number? "))

    print("Congratulations, you found it")

    return

if __name__ == "__main__": main()
