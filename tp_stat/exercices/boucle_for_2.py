#!/usr/bin/env python3
# -*- coding: utf-8
# Alexandre Gérault
# Octobre 2018

import title as tt

def fact_rec(n):
    if n > 1:
        return n*fact_rec(n-1)
    else:
        return 1

def fact_it(n):
    result = 1
    
    for i in range(1,n+1):
        result *= i

    return result

def main():

    debug = True

    if debug:
        tt.show(tt.create("Factorial computation"))


    N = int(input("Integer: "))

    print("Factorial of {} is {} using the for loop".format(N,fact_it(N)))
    print("Factorial of {} is {} using the recursive function".format(N,fact_rec(N)))

    return

if __name__ == "__main__": main()
    
