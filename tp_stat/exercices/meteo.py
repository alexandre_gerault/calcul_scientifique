#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Samedi 20 octobre

import matplotlib.pyplot as plt
import title as tt
import sys

# debug = False

# if "-d" in sys.argv:
#  debug = True

def my_debug(name):
  if debug:
    print("DEBUG: enter in {}".format(name))

  return

def list_max(my_list_):
  max = my_list_[0]
  for i in my_list_:
    if i > max: max = i
  
  return max

def list_min(my_list_):
  min = my_list_[0]
  for i in my_list_:
    if i < min: min = i
    
  return min

def list_average(my_list_):
  sum = 0
  for i in my_list_:
    sum += i

  return sum/len(my_list_)

def list_standart_deviation(my_list_):
  n = len(my_list_)
  i_average = list_average(my_list_)
  sum_deviations_average = 0
  for i in my_list_:
    sum_deviations_average += (i-i_average)**2
  
  return ((1/n)*sum_deviations_average)**0.5

def process_file(filename_):
  load_file = open(filename_, 'r')
  values = []
  for line in load_file:
    value = float(line.rstrip())
    values.append(value)
  
  min_value = list_min(values)
  max_value = list_max(values)
  avg_value = list_average(values)
  std_dvt_avg = list_standart_deviation(values)

  load_file.close()

  return values, min_value, max_value, avg_value, std_dvt_avg

def print_list(my_list_,name_):
  print("{} START".format(name_))
  for elt in my_list_:
    print(elt)
  print("{} END".format(name_))

def sort_list_crs(my_list_):
  sorted_list = []
  while len(my_list_) > 1:
    min = list_min(my_list_)
    sorted_list.append(min)
    del my_list_[my_list_.index(min)]
  
  return sorted_list

def save(filename_, min_, max_, avg_, std_avg_):
  save_file = open(filename_, 'w')
  save_file.write("Min: {}\nMax: {}\nAverage: {}\nStandard deviation: {}".format(min_, max_, avg_, std_avg_))
  save_file.close()

  return

def draw(title_, my_list_):
  plt.figure()
  plt.title(title_)
  plt.plot(my_list_, 'o')
  plt.show(block=False)

  return

def main():

  if debug:
    tt.show(tt.create("Meteo processing"))
  
  temp_values, temp_min, temp_max, temp_avg, temp_std_avg = process_file('temperature.dat')
  press_values, press_min, press_max, press_avg, press_std_avg = process_file('pression.dat')
  humidity_values, humidity_min, humidity_max, humidity_avg, humidity_std_avg = process_file('humidite.dat')

  save('temperature.yml', temp_min, temp_max, temp_avg, temp_std_avg)
  save('pression.yml', press_min, press_max, press_avg, press_std_avg)
  save('humidity.yml', humidity_min, humidity_max, humidity_avg, humidity_std_avg)

  temp_values = sort_list_crs(temp_values)
  press_values = sort_list_crs(press_values)
  humidity_values = sort_list_crs(humidity_values)

  ###################
  # PLOTITNG VALUES #
  ###################
  draw('Températures', temp_values)
  draw('Pression', press_values)
  draw('Humidité', humidity_values)

  plt.show()
  
if __name__ == "__main__":

  debug = False

  if "-d" in sys.argv:
    debug = True

  my_debug("DEBUG TEST")
  main()
