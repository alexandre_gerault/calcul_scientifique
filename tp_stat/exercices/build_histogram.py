#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Vendredi 19 octobre 2018

import matplotlib.pyplot as plt
import numpy as np
import title as tt

def main():

  debug = True

  if debug:
    tt.show(tt.create("Building an histogram"))

  ##############################
  # Defining all our variables #
  ##############################
  data_file = open('data.dat', 'r')
  min, max, nb_bin = int(input("Min: ")), int(input("Max: ")), int(input("Number of classes: "))

  histo_bins = [0]*nb_bin

  bin_width = (max - min)/(nb_bin)

  bins = np.arange(min, max, bin_width)


  data_lines = data_file.readlines()
  data_list = []

  ###################
  # Resuming inputs #
  ###################  
  print("---------INPUTS----------\n* Min: {}\n* Max: {}\n* Number of classes: {}\n* Bin width: {}".format(min, max, nb_bin, bin_width))
  print("Bins: {}".format(bins))
  #######################################################
  # We fill our data list (stripping each \n character) #
  #######################################################
  for line in data_lines:
    line_stripped = line.rstrip()
    data_list.append(float(line_stripped))


  print(type(data_list))
  ###########################
  # Processing all our data #
  ###########################
  #iterator = 0
  #for bin in bins:
  #  for data in data_list:
  #    if data < bin:
  #      data_list.remove(data)
  #      histo_bins[iterator] += 1

  #  iterator += 1

  ###################################
  # Displaying our data with pyplot #
  ###################################

  plt.hist(data_list, nb_bin)
  #plt.plot(bins, histo_bins, '*-')

  plt.show()

  return

if __name__ == "__main__": main()
