#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Gerault Alexandre
# Octobre 2018

import title as tt

def main():

    debug = True
    odds  = []
    evens = []
    
    if debug:
        tt.show(tt.create("Exercice sur les boucles for"))

    N = int(input("Entrer un nombre positif: "))

    choice = input("Voulez les choix pairs ou impairs (entrez p ou i)")

    if choice == "p":
        pair_file = open("pairs.dat", "w")
        for x in range (0,N):
            odd = 2*x
            odds.append(odd)
            pair_file.write("{}\n".format(str(odd)))
        print("Written numbers in file: {}".format(odds))
    elif choice == "i":
        unpair_file = open("unpairs.dat", "w")
        for x in range (0,N):
            even = 2*x+1
            evens.append(even)
            unpair_file.write("{}\n".format(str(even)))
        print("Written numbers in file: {}".format(evens))
        
    return


if __name__ == "__main__": main()
