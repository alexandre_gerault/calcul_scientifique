#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Vendredi 16 novembre 2018

import sys
        
debug = False

def main():
    nb_bins = 12
    xmins = [-273.15,0.0,2.0,4.0,6.0,8.0,10.0,12.0,14.0,16.0,18.0,20.0]
    xmaxs = xmins[1:];xmaxs.append(100.0)
    counts = [0]*nb_bins
    my_debug("Xmins: {}\nXmaxs: {}".format(xmins,xmaxs))

    T0 = 6.25
    ibin = None
    for i in range(nb_bins):
        if T0 >= xmins[i] and T0 < xmaxs[i]:
            ibin = i
            break
    my_debug("ibin: {}".format(ibin))

    counts[i]+=1

    my_debug("Counts: {}".format(counts))
        
    return

def my_debug(string):
    if debug:
        print(string)
    return

if __name__ == "__main__":
    if "-d" in sys.argv:
        print("Debug activated")
        debug = True
        
    main()
