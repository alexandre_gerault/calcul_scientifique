#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# Vendredi 16 novembre 2018

import sys
        
debug = False

def main():
    nb_bins = 12
    xmins = [-273.15,0.0,2.0,4.0,6.0,8.0,10.0,12.0,14.0,16.0,18.0,20.0]
    xmaxs = xmins[1:];xmaxs.append(100.0)
    counts = [0]*nb_bins
    my_debug("Xmins: {}\nXmaxs: {}".format(xmins,xmaxs))

    T0 = 206.25
    ibin = histogram_find_bin_index(nb_bins, xmins, xmaxs, T0)

    my_debug("ibin = {}".format(ibin))

    if ibin!=None: counts[ibin]+=1
    
    my_debug("Counts: {}".format(counts))
        
    return

def my_debug(string):
    if debug:
        print(string)
    return

def histogram_find_bin_index(h_nb_bins, h_bin_infs, h_bin_sups, value):
    for i in range(h_nb_bins):
        if value > h_bin_infs[i] and value <= h_bin_sups[i]:
            return i
    

if __name__ == "__main__":
    if "-d" in sys.argv:
        print("Debug activated")
        debug = True
        
    main()
