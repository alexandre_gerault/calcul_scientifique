#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Lemiere Yves (edited by Alexandre Gérault)
# Juillet 2017


import matplotlib.pyplot as plt
import title as tt
import numpy as np

def exemple():
    debug = True

    if debug:
        print("*******************")
        print("* Welcome in plot *")
        print("*******************\n")


    x = [1,2,3,4,5,6,7,8,9,10]
    y = [1.81,1.64,1.48,1.34,1.21,1.10,0.99,0.90,0.81,0.74]

    # Le graphe sera construit à partir des listes x et y avec l'option d'affichage o
    plt.plot(x,y,'o-')

    # Ajout des titres du graphe
    plt.xlabel('X[m]')
    plt.ylabel('Y[m]')
    plt.title('Titre modifié')

    # Ajustement des echelles
    plt.axis([0, 12, 0, 2])
    # Préparation d'une grille
    plt.grid(True)

    # Affichage de la fenetre contenant le graphe
    plt.show()

def main():
    
    debug = True

    if debug:
        tt.show(tt.create("Working with matplotlib"))
    
    x = np.arange(0,7,0.1)
    y_sin = []
    y_cos = []

    for i in x:
        y_sin.append(np.sin(i))
        y_cos.append(np.cos(i))
    
    plt.plot(x, y_cos, 'b-')
    plt.plot(x, y_sin, 'g-')

    plt.xlabel("$x$")
    plt.ylabel("$y$")

    plt.title('Courbe $\sin (x)$ et $\cos (x)$')
    plt.axis([0, 7, -1, 1])
    plt.grid(True)

    plt.show()

if __name__ == "__main__": main()