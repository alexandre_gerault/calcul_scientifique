#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Alexandre Gérault
# 19 octobre 2018

import matplotlib.pyplot as plt
import title as tt

def main():
    debug = True

    if debug:
        tt.show(tt.create("Génération de la carte de France"))
    
    map_file = open('map.dat', 'r')
    river_file = open('river.dat', 'r')

    map_x, map_y = [], []
    river_x, river_y = [], []
    
    for line in map_file:
        splitted_line = line.split(" ")
        map_x.append(float(splitted_line[0]))
        map_y.append(float(splitted_line[1]))

    for line in river_file:
        splitted_line = line.split(" ")
        river_x.append(float(splitted_line[0]))
        river_y.append(float(splitted_line[1]))

    plt.plot(river_x,river_y, 'b,')
    plt.plot(map_x,map_y, 'k,')

    plt.show()

    return

if __name__ == "__main__": main()