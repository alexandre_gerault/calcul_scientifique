#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Lemiere Yves
# Juillet 2017


def bidon(arg0,num):

    num = arg0+2
    arg0 = 0
    return num,arg0

def main():
    debug = False

    if debug:
        print("***********************")
        print("* Welcome in exo_list *")
        print("***********************\n")

        my_list_of_heroes = ['Spider-Man','Daredevil','Iron Man','Flash','Wonder Woman']

        heroes_with_powers = []

        heroes_with_powers.extend(my_list_of_heroes)
        heroes_with_powers.remove("Iron Man")
        print(my_list_of_heroes)


        heroes_with_money = [my_list_of_heroes[my_list_of_heroes.index("Iron Man")]]
        print("Heroes with powers: {}\nHeroes without powers but with money: {}".format(heroes_with_powers,heroes_with_money))
        

        print(my_list_of_heroes)

        # Un héros est ajouté à la fin de la "list"
        my_list_of_heroes.append("Hulk")
        print(my_list_of_heroes)

        # Le héros 'Wonder Woman' sera supprimé de la "list"
        my_list_of_heroes.remove("Wonder Womn")

        # Le héros à la première position sera supprimé de la "list"
        del my_list_of_heroes[0]

        print(my_list_of_heroes)


        for hero in my_list_of_heroes:
            print ("my current hero is {}".format(hero))

            
        my_second_list_of_heroes = ["À Dèle Blanc-Sec","Ma Ya l'abeille","Yaka Rit Jaune"]
        my_list_of_heroes.extend(my_second_list_of_heroes)

        # Display first and last elements of my list of heroes
        print("First element of my list of heroes: {}\nLast element of my list of heroes: {}".format(my_list_of_heroes[0],my_list_of_heroes[len(my_list_of_heroes)-1]))

        my_list_of_heroes = []
        print("My list of heroes: {}".format(my_list_of_heroes))
        print("Finished...")


    val1 = 666
    val2 = 42
    val3 = 12
    
    val2,val3 = bidon(val1,val2)
    
    print(val2)
    print(val3)
        




    return




    
main()
